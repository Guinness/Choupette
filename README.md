# Choupette, a score counter module for sopel.

A small counter module, stores points for each user on a chan.
Points are counted by channel.

# Informations

Author: Rémi Oudin
Date: 14/02/20176
License: GPLv3+
