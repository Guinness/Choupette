#!/usr/bin/python3
#-*- coding: utf-8 -*-
"""
@author: Rémi Oudin
@date: 2017-05-13
@license: GPLv3
@brief: Choupette, a Sopel module that stores scores in a channel.
"""

from os.path import join, isfile
import json
from sopel import module

#Prefix of Nicknames
PRIV_PREFIX = ["@", "&", "~", "%"]

def get_filename(self, channel):
    """ Get a file name. Either the users, or the scores"""
    name = self.nick + "-" + channel + ".score"
    return join(self.config.core.homedir, name)


def get_scores(self, channel):
    """ Get the dictionnary containing the scores"""
    scorefile = open(get_filename(self, channel), mode='r+')
    data = json.load(scorefile)
    return scorefile, data

@module.commands('update')
def update(bot, trigger):
    """ Updates the list of nicks registered, by scanning the nicks, and adding
    the new ones.
    """
    users = [str(user) for user in bot.privileges[trigger.sender].keys()]
    users.remove(bot.nick)
    filename = get_filename(bot, trigger.sender)

    if isfile(filename):
        with open(filename, mode='r+') as scores:
            data_scores = json.load(scores)
            scores.seek(0)
            for user in users:
                if str(user) not in data_scores.keys():
                    data_scores.update({user : 0})
            json.dump(data_scores, scores)
            scores.truncate()
    else:
        with open(filename, mode='w') as new_scores:
            data = {user: 0 for user in users}
            json.dump(data, new_scores)
    bot.say("Updated files: users known are %s." % ", ".join(users))


@module.commands('point')
def point_taken(bot, trigger):
    """ Adds a point to the given Nickname """
    score_f, scores = get_scores(bot, trigger.sender)
    nick = trigger.group(2)
    if not nick:
        bot.say("Well, please give a nickname to score")
        return 0
    nick = nick.replace(' ', '')
    if scores.get(nick, None) is None:
        nicks = [str(user) for user in bot.privileges[trigger.sender].keys()]
        nicks.remove(str(bot.nick))
        if nick not in nicks:
            bot.say("Hey, %s is not on the chan, I can't be fooled..." % nick)
            return 0
        update(bot, trigger)
        score_f.close()
        score_f, scores = get_scores(bot, trigger.sender)
    scores[nick] += 1
    score_f.seek(0)
    json.dump(scores, score_f)
    score_f.close()
    score(bot, trigger)

@module.commands('score')
def score(bot, trigger):
    """ Sends the current scores of present characters."""
    nicks = [str(user) for user in bot.privileges[trigger.sender]]
    _, scored = get_scores(bot, trigger.sender)
    fmt_string = " - ".join("%s : %d" %(nick, score) for (nick, score) in
                           scored.items() if nick in nicks)
    bot.say("Score is %s" % fmt_string)
